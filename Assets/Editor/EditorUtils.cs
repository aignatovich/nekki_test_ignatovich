﻿using UnityEditor;

public static class EditorUtils
{
    [MenuItem("Assets/Copy path")]
    static void CopyAssetPath()
    {
        var guids = Selection.assetGUIDs;
        if (guids.Length > 0)
        {
            var guid = guids[0];
            var path = AssetDatabase.GUIDToAssetPath(guid);
            EditorGUIUtility.systemCopyBuffer = path;
        }
    }
}
