﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class UnitsModule
{
    public event Action<int> UnitDeadEvent;
    public int ControlledUnitId { get; private set; }
    public Dictionary<int, Unit> Units { get; private set; }

    private IBattleContext battleContext;
    private IdGenerator idGenerator;
    private GameObject root;

    public UnitsModule(IBattleContext battleContext, IdGenerator idGenerator)
    {
        this.battleContext = battleContext;
        this.idGenerator = idGenerator;
        Units = new Dictionary<int, Unit>();
        root = new GameObject("Units");
    }

    public Unit CreateUnit(string defId, Team team, Vector3 pos)
    {
        var definition = battleContext.Definitions.GetDefinition<UnitDefinition>(defId);
        if (definition == null)
        {
            Debug.LogError("definition == null");
            return null;
        }

        var unit = battleContext.Pool.Spawn<Unit>(definition.PathToPrefab, pos, 0);
        unit.transform.parent = root.transform;
        var unitId = idGenerator.GenerateId();
        unit.Init(unitId, defId, team, battleContext);
        BuildUnitScript(unit, definition);
        Units.Add(unitId, unit);
        return unit;
    }

    public void SetControlledUnit(int unitId)
    {
        Unit unit = null;
        if (Units.TryGetValue(unitId, out unit))
        {
            ControlledUnitId = unitId;
            unit.ChangeManager(ManagersType.Strategy, BuildStrategy(StrategyType.UserControll, unit));
        }
    }

    private void BuildUnitScript(Unit unit, UnitDefinition def)
    {
        unit.AddManager(ManagersType.Move, CreateMoveComponent(unit));
        unit.AddManager(ManagersType.Strategy, (BuildStrategy( def.DefaultStrategy, unit)));
        unit.AddManager(ManagersType.Weapon, CreateWeaponComponent(unit, def.Weapons));
    }

    private StrategyManager BuildStrategy(StrategyType strategyType,  Unit host)
    {
        StrategyManager strategy = null;

        switch (strategyType)
        {
            case StrategyType.Empty:
                strategy = new StrategyManager(host, battleContext);
                break;
            case StrategyType.UserControll:
                strategy = new PlayerControllStrategy(host, battleContext);
                break;
            case StrategyType.Monster:
                strategy = new MonsterStrategy(host, battleContext);
                break;
        }

        if (strategy == null)
        {
            Debug.LogError("strategy == null");
        }

        return strategy;
    }
    
    private MoveManager CreateMoveComponent(Unit unit)
    {
        return new MoveManager(unit, battleContext);
    }

    private WeaponsManager CreateWeaponComponent(Unit unit, string[] weapons )
    {
        return new WeaponsManager(unit, weapons, battleContext);
    }

    public void KillUnit(int unitId)
    {
        var unit = Units[unitId];
        //Todo не очищать компоненты, а переиспользовать
        unit.Clear();
        battleContext.Pool.Despawn(battleContext.Definitions.GetDefinition<UnitDefinition>(unit.DefinitionId).PathToPrefab,  unit.gameObject);
        Units.Remove(unitId);
        if (UnitDeadEvent != null)
        {
            UnitDeadEvent(unitId);
        }
    }

    public void PlayerShot()
    {
        Unit unit = null;
        if (Units.TryGetValue(ControlledUnitId, out unit))
        {
            unit.GetManager<PlayerControllStrategy>(ManagersType.Strategy).Shot();
        }
    }

    public void IncreaseSpeedControlledUnit(PlayerInputModule.IncreaseSpeedMessage increaseSpeedMessage)
    {
        Unit unit = null;
        if (Units.TryGetValue(ControlledUnitId, out unit))
        {
            unit.GetManager<PlayerControllStrategy>(ManagersType.Strategy).IncreaseSpeed(increaseSpeedMessage);
        }
    }

    public void RotateControlledUnit(PlayerInputModule.RotateMessage rotateMessage)
    {
        Unit unit = null;
        if (Units.TryGetValue(ControlledUnitId, out unit))
        {
            unit.GetManager<PlayerControllStrategy>(ManagersType.Strategy).Rotate(rotateMessage);
        }
    }

    public void ChangeWeapon(PlayerInputModule.ChangeWeaponMessage changeWeaponMessage)
    {
        Unit unit = null;
        if (Units.TryGetValue(ControlledUnitId, out unit))
        {
            unit.GetManager<PlayerControllStrategy>(ManagersType.Strategy).ChangeWeapon(changeWeaponMessage);
        }
    }
}