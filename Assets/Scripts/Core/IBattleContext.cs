﻿public interface IBattleContext
{
    GameObjectsPool Pool { get; }
    DefinitionsStorage Definitions { get; }
    UnitsModule Units { get; }
    ProjectilesModule Projectiles { get; }
    MissionDataStorage MissionDataStorage { get; }
}