﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BattleScenary
{
    private IBattleContext battleContext;
    private Spawner[] spawners;

    public BattleScenary(IBattleContext battleContext)
    {
        this.battleContext = battleContext;
    }

    public void Start()
    {
        var missionData = battleContext.MissionDataStorage.MissionData;
        spawners = new Spawner[missionData.EnemyWaveData.Length + 1];
        spawners[0] = new PlayerSpawner(battleContext, missionData.PlayerUnit);
        spawners[0].Start();

        for (int i = 0; i < missionData.EnemyWaveData.Length; ++i)
        {
            spawners[i + 1] = new MonsterSpawner(battleContext, missionData.EnemyWaveData[i]);
            spawners[i + 1].Start();
        }
    }

    public void Execute(float dt)
    {
        for (int i = 0; i < spawners.Length; ++i)
        {
            spawners[i].Execute(dt);
        }
    }

    public void Stop()
    {
        for (int i = 0; i < spawners.Length; ++i)
        {
            spawners[i].Stop();
        }
    }

    #region Spawners
    private abstract class Spawner
    {
        public abstract void Start();
        public abstract void Execute(float dt);
        public abstract void Stop();
    }

    private class MonsterSpawner : Spawner
    {
        private MissionData.WaveData waveData;
        private float timeToNextSpawn;
        private IBattleContext battleContext;
        private List<int> spawnedUnits = new List<int>();

        public MonsterSpawner(IBattleContext battleContext, MissionData.WaveData waveData)
        {
            this.battleContext = battleContext;
            this.waveData = waveData;
            timeToNextSpawn = waveData.SpawnDelay;
        }

        public override void Start()
        {
            for (int i = 0; i < waveData.MinEnemyUnits; ++i)
            {
                int index = Random.Range(0, waveData.Units.Length);
                SpawnNewUnit(waveData.Units[index]);
            }

            battleContext.Units.UnitDeadEvent += OnUnitDead;
        }

        public override void Execute(float dt)
        {
            timeToNextSpawn -= dt;

            if (timeToNextSpawn <= 0 && spawnedUnits.Count < waveData.MaxUnitCount)
            {
                int index = Random.Range(0, waveData.Units.Length);
                SpawnNewUnit(waveData.Units[index]);
            }
        }

        public override void Stop()
        {
            battleContext.Units.UnitDeadEvent -= OnUnitDead;
        }

        private void SpawnNewUnit(MissionData.SpawnData spawnData)
        {
            string defId = GetRandomDefId(waveData);
            Vector3 pos = GetPosition(battleContext, spawnData);
            var unit = battleContext.Units.CreateUnit(defId, Team.B, pos);
            timeToNextSpawn = waveData.SpawnDelay;
            spawnedUnits.Add(unit.Id);
        }

        private string GetRandomDefId(MissionData.WaveData waveData)
        {
            var index = Random.Range(0, waveData.Units.Length);
            return waveData.Units[index].DefinitionId;
        }

        private Vector3 GetPosition(IBattleContext battleContext, MissionData.SpawnData spawnData)
        {
            Vector3 position = Vector3.zero;
            switch (spawnData.SpawnPositionType)
            {
                case SpawnPositionType.FixedPosition:
                    position = spawnData.Pos;
                    break;

                case SpawnPositionType.RandomOutMap:
                    {
                        var rect = battleContext.MissionDataStorage.MissionData.MapRect;
                        var x = Random.Range(-rect.width / 2, rect.width / 2);
                        var z = Random.Range(-rect.height / 2, rect.height / 2);

                        if (x > z)
                        {
                            z = Mathf.Sign(z) * (rect.height / 2 + 1);
                        }
                        else
                        {
                            x = Mathf.Sign(x) * (rect.width / 2 + 1);
                        }

                        position = new Vector3(x, 0, z);
                    }
                    break;
            }

            return position;
        }

        private void OnUnitDead(int unitId)
        {
            if (spawnedUnits.Contains(unitId))
            {
                spawnedUnits.Remove(unitId);
            }
        }
    }

    private class PlayerSpawner : Spawner
    {
        private IBattleContext battleContext;
        private MissionData.SpawnData playerUnitData;
        private int playerId;

        public PlayerSpawner(IBattleContext battleContext, MissionData.SpawnData playerUnitData)
        {
            this.battleContext = battleContext;
            this.playerUnitData = playerUnitData;
        }

        public override void Start()
        {
            battleContext.Units.UnitDeadEvent += OnUnitDead;
            Spawn();
        }

        public override void Execute(float dt)
        {

        }

        public override void Stop()
        {
            battleContext.Units.UnitDeadEvent -= OnUnitDead;
        }

        private void Spawn()
        {
            var unit = battleContext.Units.CreateUnit(playerUnitData.DefinitionId, Team.A, playerUnitData.Pos);
            playerId = unit.Id;
            battleContext.Units.SetControlledUnit(unit.Id);
        }

        private void OnUnitDead(int id)
        {
            if (id == playerId)
            {
                Spawn();
            }
        }
    }
    #endregion
}

