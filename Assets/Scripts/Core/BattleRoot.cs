﻿using UnityEngine;

public class BattleRoot : MonoBehaviour, IBattleContext
{
    #region IBattleContext
    public GameObjectsPool Pool { get; private set; }
    public DefinitionsStorage Definitions { get; private set; }
    public UnitsModule Units { get; private set; }
    public ProjectilesModule Projectiles { get; private set; }
    public MissionDataStorage MissionDataStorage { get; private set; }
    #endregion

    [SerializeField] private PlayerInputModule _playerInput;

    private CameraModule cameraModule;
    private BattleScenary battleScenary;

    void Start ()
    {
        Definitions = new DefinitionsStorage();
        Definitions.LoadUnitDefinitions();
        MissionDataStorage = new MissionDataStorage();
        MissionDataStorage.LoadMissionsData();
        Pool = new GameObjectsPool();
        IdGenerator idGenerator = new IdGenerator();
        Units  = new UnitsModule(this, idGenerator);
        Projectiles = new ProjectilesModule(this, idGenerator);
        cameraModule = FindObjectOfType<CameraModule>();

        _playerInput = new PlayerInputModule();
        _playerInput.ShotEvent += OnPlayerShot;
        _playerInput.ChangeWeaponEvent += OnPlayerChangeWeapon;
        _playerInput.IncreaseSpeedEvent += OnIncreaseSpeed;
        _playerInput.RotateEvent += OnRotate;

        battleScenary = new BattleScenary(this);
        battleScenary.Start();

        cameraModule.SetupTarget(Units.Units[Units.ControlledUnitId].transform);
    }

    void Update()
    {
        _playerInput.Execute(Time.deltaTime);
        battleScenary.Execute(Time.deltaTime);
    }

    #region userinput
    private void OnPlayerShot()
    {
        Units.PlayerShot();
    }

    private void OnPlayerChangeWeapon(PlayerInputModule.ChangeWeaponMessage arg)
    {
        Units.ChangeWeapon(arg);
    }

    private void OnIncreaseSpeed(PlayerInputModule.IncreaseSpeedMessage arg)
    {
        Units.IncreaseSpeedControlledUnit(arg);
    }

    private void OnRotate(PlayerInputModule.RotateMessage arg)
    {
        Units.RotateControlledUnit(arg);
    }
    #endregion

    private void OnDestroy()
    {
        battleScenary.Stop();

        _playerInput.ShotEvent -= OnPlayerShot;
        _playerInput.IncreaseSpeedEvent -= OnIncreaseSpeed;
        _playerInput.RotateEvent -= OnRotate;
        _playerInput.ChangeWeaponEvent -= OnPlayerChangeWeapon;
    }
}
