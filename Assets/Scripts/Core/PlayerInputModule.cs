﻿using System;
using UnityEngine;

public class PlayerInputModule
{
    public event Action ShotEvent;

    public event Action<ChangeWeaponMessage> ChangeWeaponEvent; 
    public event Action<IncreaseSpeedMessage> IncreaseSpeedEvent;
    public event Action<RotateMessage> RotateEvent;

    public void Execute(float dTime)
    {
        if (ShotEvent != null && Input.GetAxis(Constants.FIRE_BUTTON) != 0)
        {
            ShotEvent();
        }

        if (ChangeWeaponEvent != null)
        {
            if (Input.GetButtonDown("PrevWeapon"))
            {
                ChangeWeaponEvent(new ChangeWeaponMessage(dTime, -1));
            }

            if (Input.GetButtonDown("NextWeapon"))
            {
                ChangeWeaponEvent(new ChangeWeaponMessage(dTime, 1));
            }
        }

        if (IncreaseSpeedEvent != null)
        {
            float speed = Input.GetAxis(Constants.VERTICAL_AXIES);
            if (speed != 0)
            {
                IncreaseSpeedEvent(new IncreaseSpeedMessage(dTime, speed));
            }
        }

        if (RotateEvent != null)
        {
            float rotation = Input.GetAxis(Constants.HORIZONTAL_AXIES);
            if (rotation != 0)
            {
                RotateEvent(new RotateMessage(dTime, rotation));
            }
        }
    }

    public struct IncreaseSpeedMessage
    {
        public float DeltaTime;
        public float Speed;

        public IncreaseSpeedMessage(float dTime, float speed)
        {
            DeltaTime = dTime;
            Speed = speed;
        }
    }

    public struct RotateMessage
    {
        public float DeltaTime;
        public float Rotate;

        public RotateMessage(float dTime, float rotate)
        {
            DeltaTime = dTime;
            Rotate = rotate;
        }
    }

    public struct ChangeWeaponMessage
    {
        public float DeltaTime;
        public float Direction;

        public ChangeWeaponMessage(float dTime, float direction)
        {
            DeltaTime = dTime;
            Direction = direction;
        }
    }
}