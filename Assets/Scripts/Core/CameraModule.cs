﻿using UnityEngine;

public class CameraModule : MonoBehaviour
{
    [SerializeField] private float height = 10;

    private Transform target;

    public void SetupTarget(Transform target)
    {
        this.target = target;
    }

    void LateUpdate()
    {
        if (target == null)
        {
            return;
        }

        transform.position = new Vector3(target.position.x, target.position.y + height, target.transform.position.z);
    }
}
