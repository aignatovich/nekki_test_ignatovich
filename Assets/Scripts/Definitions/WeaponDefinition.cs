﻿using UnityEngine;

public abstract class WeaponDefinition : BaseDefinition
{
    [SerializeField] private float damage;
    [SerializeField] private float reloadTime;

    public abstract WeaponType WeaponType { get; }

    public float Damage
    {
        get { return damage; }
    }

    public float ReloadTime
    {
        get { return reloadTime; }
    }
}
