﻿using UnityEngine;

public class UnitDefinition : BaseDefinition
{
    [SerializeField] private float speed;
    [SerializeField] private float angularSpeed;
    [SerializeField] StrategyType defaultStrategy;
    [SerializeField] private float hp;
    [Range(0,1)]
    [SerializeField] private float armor;
    [SerializeField] private string[] weapons;

    public StrategyType DefaultStrategy
    {
        get { return defaultStrategy; }
    }

    public float Speed
    {
        get { return speed; }
    }

    public float AngularSpeed
    {
        get { return angularSpeed; }
    }

    public float Armor
    {
        get { return armor; }
    }

    public float HP
    {
        get { return hp; }
    }

    public string[] Weapons
    {
        get { return weapons; }
    }
}
