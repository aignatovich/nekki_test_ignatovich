﻿using System;
using UnityEngine;

[Serializable]
public abstract class BaseDefinition : ScriptableObject
{
    [SerializeField] string pathToPrefab;
    [SerializeField] string definitionId;

    public string PathToPrefab
    {
        get { return pathToPrefab; }
    }
    public string DefinitionId
    {
        get { return definitionId; }
    }
}
