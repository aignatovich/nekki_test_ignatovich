﻿public class MeleeWeaponDefenition : WeaponDefinition
{
    public override WeaponType WeaponType
    {
        get { return WeaponType.Melee; }
    }
}
