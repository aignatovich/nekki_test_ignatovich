﻿using UnityEngine;

public class RangeWeaponDefinition : WeaponDefinition
{
    [SerializeField] private string projectilePrefabPath;
    [SerializeField] private float projectileTTL;
    [SerializeField] private float projectileSpeed;

    public override WeaponType WeaponType
    {
        get { return WeaponType.Range;}
    }

    public float ProjectileTTL
    {
        get { return projectileTTL; }
    }

    public float ProjectileSpeed
    {
        get { return projectileSpeed; }
    }

    public string ProjectilePrefabPath
    {
        get { return projectilePrefabPath; }
    }
}
