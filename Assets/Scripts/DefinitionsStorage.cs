﻿using System.Collections.Generic;
using UnityEngine;

public class DefinitionsStorage
{
    private Dictionary<string, BaseDefinition>  Definitions;

    public DefinitionsStorage()
    {
        Definitions = new Dictionary<string, BaseDefinition>();
    }

    public void LoadUnitDefinitions()
    {
        var loadedDefinitions = Resources.LoadAll<BaseDefinition>(Constants.PathToDefinitions);
        for (int i = 0; i < loadedDefinitions.Length; ++i)
        {
            Definitions.Add(loadedDefinitions[i].DefinitionId, loadedDefinitions[i]);
        }
    }

    public T GetDefinition<T>(string defId) where T : BaseDefinition
    {
        BaseDefinition def;
        if (Definitions.TryGetValue(defId, out def))
        {
            return def as T;
        }

        Debug.LogError(string.Format("Cant find def {0}", defId));
        return null;
    }

    public BaseDefinition GetDefinition(string defId) 
    {
        BaseDefinition def = null;
        Definitions.TryGetValue(defId, out def);
        return def;
    }
}
