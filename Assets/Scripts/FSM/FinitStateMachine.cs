﻿using System.Collections.Generic;

public class FinitStateMachine<TContext, T> where T : BaseState<TContext>
{
    private Dictionary<string, T> states = new Dictionary<string, T>();
    private IState currentState;

    public void AddState(string nameOfState, T newState)
    {
        states.Add(nameOfState, newState);
    }

    public void SetStartState(string nameOfStartState)
    {
        currentState = states[nameOfStartState];
        currentState.Enter();
    }

    public void SwitchState(string state)
    {
        if (currentState != null)
        {
            currentState.Exit();
        }

        currentState = states[state];
        currentState.Enter();
    }

    public void Execute(float dt)
    {
        currentState.Execute(dt);
    }
}