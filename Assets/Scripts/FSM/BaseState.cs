﻿public class BaseState<TContext> : IState
{
    protected TContext context;

    public BaseState(TContext context)
    {
        this.context = context;
    }

    public virtual void Enter()
    {
    }

    public virtual void Execute(float dt)
    {
    }

    public virtual void Exit()
    {
    }
}
