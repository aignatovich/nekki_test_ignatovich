﻿public interface IState
{
    void Enter();
    void Execute(float dt);
    void Exit();
}