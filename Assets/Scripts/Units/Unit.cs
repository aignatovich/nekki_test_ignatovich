﻿using UnityEngine;

public class Unit : BaseManagedObject
{
    #region Dynamic parametres
    public float HP{ get; private set; }
    #endregion

    public override void Init(int id, string definition, Team team, IBattleContext battleContext)
    {
        base.Init(id, definition, team, battleContext);
        AnchorsManager anchorsManager = GetComponent<AnchorsManager>();
        if (anchorsManager != null)
        {
            AddManager(ManagersType.Anchors, anchorsManager);
        }

        HP = battleContext.Definitions.GetDefinition<UnitDefinition>(DefinitionId).HP;
    }

    public void ApplyDamage(float damage)
    {
        HP -= damage*battleContext.Definitions.GetDefinition<UnitDefinition>(DefinitionId).Armor;
        if (HP <= 0)
        {
            battleContext.Units.KillUnit(Id);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(Constants.UNIT_TAG))
        {
            GetManager<StrategyManager>(ManagersType.Strategy).OnEnterCollision(other.gameObject.GetComponent<Unit>().Id);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag(Constants.UNIT_TAG))
        {
            GetManager<StrategyManager>(ManagersType.Strategy).OnExitCollision(other.gameObject.GetComponent<Unit>().Id);
        }
    }
}