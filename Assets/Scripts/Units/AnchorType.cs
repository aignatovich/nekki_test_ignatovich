﻿namespace Shared.Constants
{
    public enum AnchorType
    {
        Weapon,
        ProjectileThrow,
        UI
    }
}