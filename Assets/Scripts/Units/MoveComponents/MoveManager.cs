﻿using UnityEngine;

public class MoveManager: IManager
{
    protected Unit host;
    private IBattleContext battleContext;

    public MoveManager(Unit host, IBattleContext battleContext) 
    {
        this.host = host;
        this.battleContext = battleContext;
    }

    #region IManager
    public ManagersType ManagerType { get {return ManagersType.Move;} }

    public void Execute(float deltaTime)
    {
    }

    public void Stop()
    {
    }
    #endregion

    public void Move(float deltaTime, Vector3 direction, float velocity, bool lookAtPos = true)
    {
        if (!battleContext.MissionDataStorage.MissionData.MapRect.Contains(new Vector2(host.transform.position.x, host.transform.position.z)))
        {
            direction = Vector3.zero - host.transform.position;
            velocity = Mathf.Abs(velocity);
        }

        Vector3 moveDelta = direction.normalized * battleContext.Definitions.GetDefinition<UnitDefinition>(host.DefinitionId).Speed*deltaTime*velocity;
        Vector3 newPosition = host.transform.position + moveDelta;
        if (lookAtPos)
        {
            host.transform.LookAt(newPosition);
        }

        host.transform.position = newPosition;
    }

    public void Rotate(float deltaTime, float rotateValue)
    {
        UnitDefinition def = battleContext.Definitions.GetDefinition<UnitDefinition>(host.DefinitionId);
        float angle = deltaTime * rotateValue * def.AngularSpeed * Mathf.Rad2Deg;
        host.transform.Rotate(0, angle, 0);
    }
}
