﻿public interface IManager
{
    ManagersType ManagerType { get; }

    void Execute(float deltaTime);
    void Stop();
}

public enum ManagersType
{
    Move,
    Strategy,
    Weapon,
    Anchors,
}