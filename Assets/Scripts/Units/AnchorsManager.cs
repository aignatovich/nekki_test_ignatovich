﻿using System;
using UnityEngine;
using Shared.Constants;

public class AnchorsManager : MonoBehaviour, IManager
{
    [SerializeField] private AnchorPair[] _anchorPairs;

    #region IManager
    public ManagersType ManagerType { get {return ManagersType.Anchors;} }

    public void Execute(float deltaTime)
    {
    }

    public void Stop()
    {
    }
    #endregion

    public Transform GetAnchor(AnchorType anchorType)
    {
        for (int i = 0; i < _anchorPairs.Length; ++i)
        {
            if (_anchorPairs[i].Type == anchorType)
            {
                return _anchorPairs[i].Anchor;
            }
        }

        return transform;
    }

    public bool HasAnchor(AnchorType anchorType)
    {
        for (int i = 0; i < _anchorPairs.Length; ++i)
        {
            if (_anchorPairs[i].Type == anchorType)
            {
                return true;
            }
        }

        return true;
    }

    [Serializable]
    private struct AnchorPair
    {
        public AnchorType Type;
        public Transform Anchor;
    }
}
