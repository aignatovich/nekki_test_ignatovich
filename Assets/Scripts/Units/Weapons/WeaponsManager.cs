﻿using Shared.Constants;
using UnityEngine;

public class WeaponsManager : IManager
{
    private UnitsModule unitsModule;
    private Weapon[] weapons;
    private int currentWeaponIndex;

    public WeaponsManager(Unit host, string[] weaponsList, IBattleContext battleContext)
    {
        weapons = new Weapon[weaponsList.Length];
        for (int i = 0; i < weaponsList.Length; ++i)
        {
            weapons[i] = CreateWeapon(weaponsList[i], battleContext, host);
        }

        weapons[currentWeaponIndex].SetWeapon();
    }

    private Weapon CreateWeapon(string defId, IBattleContext battleContext, Unit host)
    {
        var def = battleContext.Definitions.GetDefinition<WeaponDefinition>(defId);
        Weapon weapon = null;
        switch (def.WeaponType)
        {
            case WeaponType.Melee:
                weapon = new MeleeWeapon(defId, battleContext, host);
                break;
            case WeaponType.Range:
                weapon = new RangeWeapon(defId, battleContext, host);
                break;
        }

        return weapon;
    }
    #region public interface
    public void Attack()
    {
        weapons[currentWeaponIndex].Attack();
    }

    public void Attack(int targetId)
    {
        weapons[currentWeaponIndex].Attack(targetId);
    }

    public void Attack(Vector3 position)
    {
        weapons[currentWeaponIndex].Attack(position);
    }

    public void NextWeapon()
    {
        weapons[currentWeaponIndex].ResetWeapon();
        currentWeaponIndex++;
        if (currentWeaponIndex >= weapons.Length)
        {
            currentWeaponIndex = 0;
        }

        weapons[currentWeaponIndex].SetWeapon();
    }

    public void PrevWeapon()
    {
        weapons[currentWeaponIndex].ResetWeapon();
        currentWeaponIndex--;
        if (currentWeaponIndex < 0)
        {
            currentWeaponIndex = weapons.Length - 1;
        }

        weapons[currentWeaponIndex].SetWeapon();
    }

    #endregion
    #region IManager
    public ManagersType ManagerType
    {
        get { return ManagersType.Weapon; }
    }

    public void Execute(float deltaTime)
    {
        for (int i = 0; i < weapons.Length; ++i)
        {
            weapons[i].Execute(deltaTime);
        }
    }

    public void Stop()
    {
        weapons[currentWeaponIndex].ResetWeapon();
    }
    #endregion

    #region Weapon
    protected abstract class Weapon
    {
        protected IBattleContext battleContext;
        protected float currentReloadTime;
        protected Unit host;
        protected GameObject weaponObject;

        protected WeaponDefinition cachedDef;

        public Weapon(string defId, IBattleContext battleContext, Unit host)
        {
            this.battleContext = battleContext;
            this.host = host;
            cachedDef = battleContext.Definitions.GetDefinition<WeaponDefinition>(defId);
            currentReloadTime = 0;
        }

        public void Execute(float dtime)
        {
            currentReloadTime -= dtime;
        }

        public abstract void Attack();
        public abstract void Attack(Vector3 pos);
        public abstract void Attack(int targetId);

        public void SetWeapon()
        {
            if (string.IsNullOrEmpty(cachedDef.PathToPrefab))
            {
                return;
            }

            Transform anchor = host.GetManager<AnchorsManager>(ManagersType.Anchors).GetAnchor(AnchorType.Weapon);

            weaponObject = battleContext.Pool.Spawn(cachedDef.PathToPrefab);
            weaponObject.transform.parent = anchor.transform;
            weaponObject.transform.localPosition = Vector3.zero;
            weaponObject.transform.localRotation = Quaternion.identity;
        }

        public void ResetWeapon()
        {
            if (weaponObject != null)
            {
                battleContext.Pool.Despawn(cachedDef.PathToPrefab, weaponObject);
            }
        }
    }

    private class RangeWeapon : Weapon
    {
        public RangeWeapon(string defId, IBattleContext battleContext, Unit host) : base(defId, battleContext, host)
        {
        }

        public override void Attack()
        {
            if (currentReloadTime <= 0)
            {
                Vector3 pos = Vector3.zero;
                float angle = 0;
                if (weaponObject != null)
                {
                    pos = weaponObject.transform.position;
                    angle = weaponObject.transform.rotation.eulerAngles.y;
                }
                else
                {
                    Transform anchor = host.GetManager<AnchorsManager>(ManagersType.Anchors).GetAnchor(AnchorType.Weapon);
                    pos = anchor.position;
                    angle = anchor.rotation.eulerAngles.y;
                }

                battleContext.Projectiles.CreateProjectile(cachedDef.DefinitionId, host.Team, pos, angle);
                currentReloadTime = cachedDef.ReloadTime;
            }
        }

        public override void Attack(Vector3 pos)
        {
            Debug.LogError("Not Implemented");
        }

        public override void Attack(int targetId)
        {
            Debug.LogError("Not Implemented");
        }
    }

    private class MeleeWeapon : Weapon
    {
        public MeleeWeapon(string defId, IBattleContext battleContext, Unit host) : base(defId, battleContext, host)
        {
        }

        public override void Attack()
        {
            Debug.LogError("Not Implemented");
        }

        public override void Attack(Vector3 pos)
        {
            Debug.LogError("Not Implemented");
        }

        public override void Attack(int targetId)
        {
            if (currentReloadTime <= 0)
            {
                Unit target = null;
                if (battleContext.Units.Units.TryGetValue(targetId, out target))
                {
                    target.ApplyDamage(cachedDef.Damage);
                    currentReloadTime = cachedDef.ReloadTime;
                }
                else
                {
                    Debug.LogError("Cant find target " + targetId);
                }
            }
        }
    }
#endregion
}