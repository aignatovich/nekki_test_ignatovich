﻿using UnityEngine;

public class PlayerControllStrategy : StrategyManager
{
    public PlayerControllStrategy(Unit host, IBattleContext battleContext) : base(host, battleContext)
    {

    }

    public void Shot()
    {
        host.GetManager<WeaponsManager>(ManagersType.Weapon).Attack();
    }

    public void IncreaseSpeed(PlayerInputModule.IncreaseSpeedMessage increaseSpeedMessage)
    {
        host.GetManager<MoveManager>(ManagersType.Move).Move(increaseSpeedMessage.DeltaTime, host.transform.forward, increaseSpeedMessage.Speed, false);
    }

    public void Rotate(PlayerInputModule.RotateMessage rotateMessage)
    {
        host.GetManager<MoveManager>(ManagersType.Move).Rotate(rotateMessage.DeltaTime, rotateMessage.Rotate);
    }

    public void Attack()
    {
        host.GetManager<WeaponsManager>(ManagersType.Weapon).Attack();
    }

    public virtual void ChangeWeapon(PlayerInputModule.ChangeWeaponMessage changeWeaponMessage)
    {
        var weaponManager = host.GetManager<WeaponsManager>(ManagersType.Weapon);
        if (changeWeaponMessage.Direction > 0)
        {
            weaponManager.NextWeapon();
        }
        else
        {
            weaponManager.PrevWeapon();
        }
    }
}