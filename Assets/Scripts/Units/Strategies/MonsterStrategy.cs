﻿using UnityEngine;

public class MonsterStrategy : StrategyManager
{
    private IBattleContext battleContext;
    private FinitStateMachine<MonsterStrategy, BaseState<MonsterStrategy>> fsm;
    private int targetUnitId;

    public MonsterStrategy(Unit host, IBattleContext battleContext ) : base(host, battleContext)
    {
        this.battleContext = battleContext;
        fsm = new FinitStateMachine<MonsterStrategy, BaseState<MonsterStrategy>>();
        fsm.AddState(StrategyStates.MoveToTarget, new GoToTarget(this));
        fsm.AddState(StrategyStates.Attack, new Attack(this));
        fsm.AddState(StrategyStates.Idle, new Idle(this));

        fsm.SetStartState(StrategyStates.MoveToTarget);
    }

    public override void Execute(float deltaTime)
    {
        fsm.Execute(deltaTime);
    }

    #region collisions
    public override void OnEnterCollision(int unitId)
    {
        Unit target;
        if (!battleContext.Units.Units.TryGetValue(unitId, out target))
        {
            return;
        }

        if (target.Team != host.Team)
        {
            targetUnitId = unitId;
            fsm.SwitchState(StrategyStates.Attack);
        }
    }

    public override void OnExitCollision(int unitId)
    {
        if (unitId == targetUnitId)
        {
            fsm.SwitchState(StrategyStates.MoveToTarget);
        }
    }
    #endregion

    #region states
    private class GoToTarget : BaseState<MonsterStrategy>
    {
        private Unit cachedTarget;
        private UnitDefinition cacheDefinition;
        private MoveManager cachedMoveManager;

        public GoToTarget(MonsterStrategy context) : base(context)
        {
           
        }

        public override void Enter()
        {
            cacheDefinition = context.battleContext.Definitions.GetDefinition<UnitDefinition>(context.host.DefinitionId);
            context.battleContext.Units.Units.TryGetValue(context.targetUnitId, out cachedTarget);
            cachedMoveManager = context.host.GetManager<MoveManager>(ManagersType.Move);
        }

        public override void Execute(float dt)
        {
            if (cachedTarget == null || cachedTarget.HP <= 0)
            {
                context.fsm.SwitchState(StrategyStates.Idle);
                return;
            }

            Vector3 distanceVector = cachedTarget.transform.position - context.host.transform.position;
            cachedMoveManager.Move(dt, distanceVector, cacheDefinition.Speed);
        }
    }

    private class Attack : BaseState<MonsterStrategy>
    {
        public Attack(MonsterStrategy context) : base(context)
        {

        }

        public override void Execute(float dt)
        {
            var weaponManager = context.host.GetManager<WeaponsManager>(ManagersType.Weapon);
            Unit unit;
            if (context.battleContext.Units.Units.TryGetValue(context.targetUnitId, out unit))
            {
                weaponManager.Attack(context.targetUnitId);
            }
            else
            {
                context.fsm.SwitchState(StrategyStates.Idle);
            }
        }
    }

    private class Idle : BaseState<MonsterStrategy>
    {
        private float timeToSearch;
        public Idle(MonsterStrategy context) : base(context)
        {

        }

        public override void Execute(float dt)
        {
            if (timeToSearch > 0)
            {
                timeToSearch -= dt;
                return;
            }

            using (var enumerator = context.battleContext.Units.Units.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current.Value.Team != context.host.Team && enumerator.Current.Value.HP > 0)
                    {
                        context.targetUnitId = enumerator.Current.Key;
                        context.fsm.SwitchState(StrategyStates.MoveToTarget);
                    }
                }
            }

            timeToSearch = 1;
        }
    }
    #endregion
}