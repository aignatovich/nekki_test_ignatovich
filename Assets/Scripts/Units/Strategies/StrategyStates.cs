﻿public static class StrategyStates
{
    public const string Undefined = "Undefined";
    public const string MoveToTarget = "MoveToTarget";
    public const string Attack = "Attack";
    public const string Idle = "Idle";
}