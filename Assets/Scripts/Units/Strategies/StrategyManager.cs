﻿public class StrategyManager : IManager
{
    protected BaseManagedObject host;
    protected IBattleContext battleContext;

    public StrategyManager(BaseManagedObject host, IBattleContext battleContext)
    {
        this.host = host;
        this.battleContext = battleContext;
    }

    #region IManager
    public ManagersType ManagerType
    {
        get { return ManagersType.Strategy; }
    }

    public virtual void Execute(float deltaTime)
    {
    }
    public void Stop()
    {
    }
    #endregion

    #region collisions
    public virtual void OnEnterCollision(int unitId)
    {
    }

    public virtual void OnExitCollision(int unitId)
    {
    }
    #endregion
}
