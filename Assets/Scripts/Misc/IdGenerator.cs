﻿using UnityEngine;
using System.Collections;

public class IdGenerator
{
    private int lastId = 0;

    public int GenerateId()
    {
        return ++lastId;
    }
}
