﻿public static class Constants
{
    #region definitions
    public const string PathToDefinitions = "Definitions";
    public const string PathToUnitDefinitions = "Definitions/Units";
    public const string PathToWeaponDefinitions = "Definitions/Weapons";
    #endregion

    #region MissionsData
    public const string PathToMissionData = "MissionsData/MissionData";
    #endregion

    #region user_input_constants
    public const string FIRE_BUTTON = "Fire";
    public const string CHANGE_WEAPON_BUTTON = "ChangeWeapon";
    public const string HORIZONTAL_AXIES = "Horizontal";
    public const string VERTICAL_AXIES = "Vertical";
    #endregion

    #region unit_id
    public const int UNDEFINED_UNIT_ID = -1;
    #endregion

    #region tags
    public const string UNIT_TAG = "Unit";
    #endregion
}
