﻿using UnityEngine;

public class MissionDataStorage
{
    public MissionData MissionData { get; private set; }

    public void LoadMissionsData()
    {
        MissionData = Resources.Load<MissionData>(Constants.PathToMissionData);
    }
}