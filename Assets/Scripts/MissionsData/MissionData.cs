﻿using System;
using UnityEngine;

public class MissionData :ScriptableObject
{
    public Rect MapRect;

    public SpawnData PlayerUnit;
    public WaveData[] EnemyWaveData;

    [Serializable]
    public class SpawnData 
    {
        public Vector3 Pos;
        public string DefinitionId;
        public SpawnPositionType SpawnPositionType;
    }

    [Serializable]
    public class WaveData 
    {
        public int MaxUnitCount;
        public int MinEnemyUnits;
        public float SpawnDelay;

        public SpawnData[] Units;
    }
}