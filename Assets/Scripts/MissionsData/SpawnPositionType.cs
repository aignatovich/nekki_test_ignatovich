﻿using System;

[Serializable]
public enum SpawnPositionType
{
    FixedPosition = 0,
    RandomOutMap = 1,
}
