﻿public class ProjectileStrategy : StrategyManager
{
    private float ttl;
    private IBattleContext battleContext;
    private Projectile host;

    public ProjectileStrategy(Projectile host, IBattleContext battleContext) : base(host, battleContext)
    {
        this.host = host;
        this.battleContext = battleContext;
        this.ttl = battleContext.Definitions.GetDefinition<RangeWeaponDefinition>(host.DefinitionId).ProjectileTTL;
    }

    #region IManager
    public override void Execute(float deltaTime)
    {
        ttl -= deltaTime;
        if (ttl <= 0)
        {
            battleContext.Projectiles.DestroyProjectile(host.Id);
        }
    }
    #endregion

    public override void OnEnterCollision(int unitId)
    {
        var damage = battleContext.Definitions.GetDefinition<WeaponDefinition>(host.DefinitionId).Damage;
        Unit unit = null;
        if (battleContext.Units.Units.TryGetValue(unitId, out unit))
        {
            if (unit.Team != host.Team)
            {
                unit.ApplyDamage(damage);
                battleContext.Projectiles.DestroyProjectile(host.Id);
            }
        }
    }
}
