﻿using UnityEngine;

public class Projectile : BaseManagedObject
{
    public override void Init(int id, string definitionId, Team team, IBattleContext battleContext)
    {
        base.Init(id, definitionId, team, battleContext);
        Id = id;
        DefinitionId = definitionId;
        this.battleContext = battleContext;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(Constants.UNIT_TAG))
        {
            GetManager<ProjectileStrategy>(ManagersType.Strategy).OnEnterCollision(other.gameObject.GetComponent<Unit>().Id);
        }
    }
}
