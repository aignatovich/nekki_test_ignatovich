﻿using System.Collections.Generic;
using UnityEngine;

public class ProjectilesModule
{
    public Dictionary<int, Projectile> Projectiles { get; private set; }

    private IBattleContext battleContext;
    private IdGenerator idGenerator;
    private GameObject root;

    public ProjectilesModule(IBattleContext battleContext, IdGenerator idGenerator)
    {
        this.battleContext = battleContext;
        this.idGenerator = idGenerator;
        Projectiles = new Dictionary<int, Projectile>();
        root = new GameObject("Projectiles");
    }

    public void CreateProjectile(string weaponDefId, Team team, Vector3 position, float rotation)
    {
        var definition = battleContext.Definitions.GetDefinition<RangeWeaponDefinition>(weaponDefId);
        if (definition == null)
        {
            Debug.LogError("definition == null");
            return;
        }

        var projectile = battleContext.Pool.Spawn<Projectile>(definition.ProjectilePrefabPath, position, rotation);
        projectile.transform.parent = root.transform;
        var id = idGenerator.GenerateId();
        projectile.Init(id, weaponDefId, team, battleContext);
        BuildScript(projectile);
        Projectiles.Add(id, projectile);
    }

    public void DestroyProjectile(int id)
    {
        Projectile projectile = null;
        if (Projectiles.TryGetValue(id, out projectile))
        {
            //Todo не очищать компоненты, а переисрользовать! 
            projectile.Clear();
            battleContext.Pool.Despawn(battleContext.Definitions.GetDefinition<RangeWeaponDefinition>(projectile.DefinitionId).ProjectilePrefabPath, projectile.gameObject);
            Projectiles.Remove(id);
        }
    }

    private void BuildScript(Projectile projectile)
    {
        projectile.AddManager(ManagersType.Strategy, BuildStrategy(projectile));
        projectile.AddManager(ManagersType.Move, BuildMoveScript(projectile));
    }

    private ProjectileStrategy BuildStrategy(Projectile host)
    {
        return new ProjectileStrategy(host, battleContext);
    }

    private ProjectileMoveManager BuildMoveScript(Projectile host)
    {
        return new ProjectileMoveManager(host, battleContext);
    }
}