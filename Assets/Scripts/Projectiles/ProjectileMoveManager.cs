﻿public class ProjectileMoveManager : IManager
{

    private Projectile host;
    private float speed;

    public ProjectileMoveManager(Projectile host, IBattleContext battleContext)
    {
        this.host = host;
        this.speed = battleContext.Definitions.GetDefinition<RangeWeaponDefinition>(host.DefinitionId).ProjectileSpeed;
    }

    #region IManager
    public ManagersType ManagerType { get { return ManagersType.Move; } }

    public void Execute(float deltaTime)
    {
        host.transform.position += host.transform.forward*deltaTime*speed;
    }

    public void Stop()
    {
    }
    #endregion
}
