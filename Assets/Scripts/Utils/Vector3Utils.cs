﻿using UnityEngine;

public static class VectorUtils
{
    public static Vector2 Rotate(this Vector2 vector, float angle)
    {
        Vector2 rotated_point;
        rotated_point.x = vector.x*Mathf.Cos(angle) - vector.y*Mathf.Sin(angle);
        rotated_point.y = vector.x*Mathf.Sin(angle) + vector.y*Mathf.Cos(angle);
        return rotated_point;
    }

    public static Vector3 Clamp(this Vector3 vector, float minMagnitude, float maxMagnitude)
    {
        float magnitude = vector.magnitude;

        if (magnitude < minMagnitude)
        {
            return (vector/magnitude)*minMagnitude;
        }

        if (magnitude > maxMagnitude)
        {
            return (vector/magnitude)*maxMagnitude;
        }

        return vector;
    }

    public static Vector2 Clamp(this Vector2 vector, float minMagnitude, float maxMagnitude)
    {
        float magnitude = vector.magnitude;

        if (magnitude < minMagnitude)
        {
            return (vector / magnitude) * minMagnitude;
        }

        if (magnitude > maxMagnitude)
        {
            return (vector / magnitude) * maxMagnitude;
        }

        return vector;
    }
}
