﻿using System.Collections.Generic;
using UnityEngine;

public class BaseManagedObject : MonoBehaviour 
{
    public int Id { get; protected set; }
    public string DefinitionId { get; protected set; }
    public Team Team { get; protected set; }

    protected IBattleContext battleContext;

    private List<IManager> _managers = new List<IManager>();

    public virtual void Init(int id, string definition, Team team, IBattleContext battleContext)
    {
        Id = id;
        DefinitionId = definition;
        Team = team;
        this.battleContext = battleContext;
    }

    void Update()
    {
        for (int i = 0; i < _managers.Count; ++i)
        {
            _managers[i].Execute(Time.deltaTime);
        }
    }

    #region set get managers
    public void AddManager(ManagersType type, IManager manager)
    {
        for (int i = 0; i < _managers.Count; ++i)
        {
            if (_managers[i].ManagerType == type)
            {
                Debug.LogError(string.Format("Unit {0} already have manager with type {1}", Id, type));
                return;
            }
        }

        _managers.Add(manager);
    }

    public T GetManager<T>(ManagersType type) where T : class, IManager
    {
        IManager manager = null;
        for (int i = 0; i < _managers.Count; ++i)
        {
            if (_managers[i].ManagerType == type)
            {
                return _managers[i] as T;
            }
        }

        //Debug.LogError(string.Format("Unit {0} dont have manager with type {1}", Id, type));
        return null;
    }

    public void ChangeManager(ManagersType type, IManager manager)
    {
        for (int i = 0; i < _managers.Count; ++i)
        {
            if (_managers[i].ManagerType == type)
            {
                _managers[i].Stop();
                _managers.RemoveAt(i);
                break;
            }
        }

        _managers.Add(manager);
    }
    #endregion

    //Todo не очищать компоненты, а переисрользовать! 
    public void Clear()
    {
        for (int i = 0; i < _managers.Count; ++i)
        {
            _managers[i].Stop();
        }
        _managers.Clear();
    }
}
